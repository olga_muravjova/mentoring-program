/**
 * Created by Ольга on 04.02.2016.
 */
$( document ).ready(function() {
    $.get( "/lists", function( data ) {
        var text = "";
        $.each(data, function(index, value) {
            text = text + " " + value.name;
        });
        $('#lists').text(text);
    });
});