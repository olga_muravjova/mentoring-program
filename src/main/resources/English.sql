﻿DROP SCHEMA IF EXISTS LE CASCADE;
CREATE SCHEMA LE;

CREATE SEQUENCE LE.THEME_IDS; 
CREATE SEQUENCE LE.Word_IDS; 
CREATE SEQUENCE LE.List_IDS; 
CREATE SEQUENCE LE.Users_IDS;
CREATE SEQUENCE LE.tags_and_words_IDS;
CREATE SEQUENCE LE.words_and_lists_IDS;
CREATE SEQUENCE LE.TEXTS_IDS;

CREATE TABLE LE.Users(id integer primary key DEFAULT NEXTVAL('LE.Users_IDS'), name varchar UNIQUE, password varchar);
CREATE TABLE LE.Lists_Of_Words(id integer primary key DEFAULT NEXTVAL('LE.List_IDS'), name varchar, user_id integer references LE.Users(id) ON DELETE CASCADE, unique (name, user_id));
CREATE TABLE LE.Tags(id integer primary key DEFAULT NEXTVAL('LE.THEME_IDS'), name varchar UNIQUE);
CREATE TABLE LE.Words(id integer primary key DEFAULT NEXTVAL('LE.Word_IDS'), name varchar UNIQUE);
CREATE TABLE LE.WORDS_AND_LISTS(id integer primary key DEFAULT NEXTVAL('LE.words_and_lists_IDS'),
 list_id integer references LE.Lists_Of_Words(id)ON DELETE CASCADE,
  word_id integer references LE.Words(id)ON DELETE CASCADE, unique(list_id, word_id));
  CREATE TABLE LE.tags_and_Words(id integer primary key DEFAULT NEXTVAL('LE.tags_and_words_IDS'),
word_id integer references LE.Words(id)ON DELETE CASCADE, 
tag_id integer references LE.Tags(id)ON DELETE CASCADE);

CREATE TABLE LE.TEXTS(id integer primary key DEFAULT NEXTVAL('LE.TEXTS_IDS'), list_id integer references LE.LISTS_OF_WORDS(id) ON DELETE CASCADE,
 title varchar, url varchar, text varchar, type varchar, check (type in('WIKIPEDIA', 'TIMES')));

INSERT INTO LE.Users(name, password) VALUES ( 'admin', '$2a$10$bL88KL.sxpcjgYudQgqYhekLQ/uu7HBCL9gLNHCO8bVwfBqBg9krO');
INSERT INTO LE.Users(name, password) VALUES ( 'olga', '$2a$10$JKZtDSA5UJ257n5cYhO/quWNlCGCZmuVQUVsXHClJxvi3pmcyV.IW');

INSERT INTO LE.Lists_Of_Words(name, user_id) VALUES ( 'food',1);
INSERT INTO LE.Lists_Of_Words(name, user_id) VALUES ( 'my_list',1);
INSERT INTO LE.Lists_Of_Words(name, user_id) VALUES ( 'favorites',1);
INSERT INTO LE.Lists_Of_Words(name, user_id) VALUES ( 'music',2);
INSERT INTO LE.Lists_Of_Words(name, user_id) VALUES ( 'music',1);

INSERT INTO LE.Tags(name) VALUES ('food');
INSERT INTO LE.Tags(name) VALUES ( 'animals');
INSERT INTO LE.Tags(name) VALUES ( 'sport');
INSERT INTO LE.Tags(name) VALUES ('music');

INSERT INTO LE.Words(name) VALUES ( 'apple');
INSERT INTO LE.Words(name) VALUES ('grapes');
INSERT INTO LE.Words(name) VALUES ( 'orange');
INSERT INTO LE.Words(name) VALUES ( 'tea');
INSERT INTO LE.Words(name) VALUES ( 'rock');
INSERT INTO LE.Words(name) VALUES ( 'pop');
INSERT INTO LE.Words(name) VALUES ( 'jazz');
INSERT INTO LE.Words(name) VALUES ( 'dog');
INSERT INTO LE.Words(name) VALUES ( 'football');
INSERT INTO LE.Words(name) VALUES ( 'basketball');
INSERT INTO LE.Words(name) VALUES ( 'golf');
INSERT INTO LE.Words(name) VALUES ( 'cat');

INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (1, 1);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (2, 2);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (3, 3);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (4, 4);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (5, 4);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (6, 1);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (7, 2);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (8, 3);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (9, 4);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (10, 4);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (1, 1);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (3, 2);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (4, 3);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (5, 4);
INSERT INTO LE.Tags_And_Words(word_id, tag_id) VALUES (2, 4);

INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (1, 1);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (1, 2);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (1, 3);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (1, 4);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (2, 8);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (2, 11);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (2, 6);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (2, 10);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (3, 12);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (3, 3);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (3, 6);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (4, 6);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (4, 5);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (4, 7);
INSERT INTO LE.WORDS_AND_LISTS(list_id, word_id) VALUES (3, 1);

INSERT INTO LE.TEXTS(list_id, title,url,text,type) VALUES (3, 'First text', 'first_text.com', 'Hello, this is first text','TIMES');
