/**
 * Created by Ольга on 14.03.2016.
 */
var userId;
//var userName = "Olga";

/**
 * document ready part
 */
$(document).ready(function () {
    //$('#title').text(userName);
    //showAllLists(userId);
    //$('#lists').empty();
    $.get("user/current/id", function (data) {
        userId = data;
    });
    $.get("user/current/name", function (data) {
        $('#userName').text(data);
    });
    $.get("/lists", function (data) {
        $.each(data, function (index, value) {
            $('#lists').append("<li class = 'list mdl-list__item' name='" + value.name + "' id =" + value.id + "><span class='mdl-list__item-primary-content'>" + value.name + "</span></li>");

        });
        showFirstList();

    });


    // $('#lists')
});

/**
 * creates a list on the left
 * @param userId
 */

function showAllLists(userId) {
    $.get("/lists", function (data) {
        $('#lists').empty();
        $.each(data, function (index, value) {
            $('#lists').append("<li class = 'list mdl-list__item' name='" + value.name + "' id =" + value.id + ">" +
                "<span class='mdl-list__item-primary-content'>" + value.name + "</span></li>");
        });
    });
}

/**
 * deletes list
 */

$(document).on('click', '#delete', function () {
    //var id = currentListId;
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'statusCode': {
            200: function () {
                var list = $('li[id = ' + currentListId + ']');
                list.remove();
                showFirstList();
                //showAllLists(userId);
            }
        },
        'type': 'DELETE',
        'url': "/list/erase",
        'data': JSON.stringify(currentListId),
        'dataType': 'json'
        //'success': function () {
        //    showAllLists(userId);
        //}
    });
});

var currentListId;
var currentListName;

/**
 * selecting current list to show info about it
 */
$(document).on('click', ".list", function () {
    //$(".word").remove();
    //$('#list_name').text($(this).attr("name"));
    currentListId = this.id.toString();
    currentListName = $(this).attr("name");
    showAllWords(currentListId, currentListName);
});

/**
 * shows info about current list on the right
 * @param listId
 * @param listName
 */
function showAllWords(listId, listName) {
    $.get("/list/" + listId + "/words", function (data) {
        currentListId = listId;
        $(".word").remove();
        $('#list_name').text(listName);
        $('#change').show();
        $('#search').show();
        $('#delete').show();
        $.each(data, function (index, value) {
            $('#words').append("<li class='word mdl-list__item'>" +
                "<span class='word_text mdl-list__item-primary-content'>" + value.name + "</span></li>");
            //$.get("/lists/tag/"+value.id, function(data){
            //    $.each(data, function (index, value) {
            //        $(this).append("<p>" +value.name+"</p>");
            //    });
            //});
        });
    });
}

/**
 * shows smth on the right(а то слишком пусто там)
 */
function showFirstList() {
    if ($('.list').length) {


        $('.list').each(function (index, value) {
            currentListId = value.id.toString();
            currentListName = $(value).attr("name");
            showAllWords(currentListId, currentListName);
            return false;
        });
    } else {
        $('#change').hide();
        $('#search').hide();
        $('#delete').hide();
        $(".word").remove();
        $('#list_name').text("You don't have any lists yet");
    }

}

var wordNames = [];

/**
 * creates new list
 */
$(document).on('click', '#save', function () {
        if ($('#name').val() == "") {
            $('#warning').text("Please enter the name of the new list");
        }
        else {
            var listName = $('#name').val();
            wordNames = [];
            $('#addModal').hide();
            $(".createInput").each(function (index, value) {
                if (value.value != "") {
                    wordNames.push(value.value);
                }
            });
            var list = {listId: null, listName: listName, words: wordNames};
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                'statusCode': {
                    202: function (data) {
                        $('#createForm').html("<label id='nameLabel'>Name</label><input id='name'> <label>Words</label><ul id = 'newWords'> " +
                            "<li class='wordInput'><span class='mdl-list__item-primary-content'><input class = 'createInput'>" +
                            " <button class='minus  mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect'>" +
                            "<i class='material-icons'>remove</i></button></span></li> </ul><label id ='warning'></label>");
                        $('#lists').append("<li class = 'list mdl-list__item' name='" + data.name + "' id =" + data.id + ">" +
                            "<span class='mdl-list__item-primary-content'>" + data.name + "</span></li>");
                        showAllWords(data.id, data.name);
                        $('#addModal').hide();
                    },
                    406: function () {
                        $('#addModal').show();
                        $('#warning').text("List with this name already exists");
                    }
                },
                'type': 'PUT',
                'url': "/list/new",
                'data': JSON.stringify(list),
                'dataType': 'json'
            });
        }
    }
);

/**
 * changes existing list
 */
$(document).on('click', '#saveChange', function () {
    var listName = $('#name').val();
    wordNames = [];

    $(".createInput").each(function (index, value) {
        if (value.value != "") {
            wordNames.push(value.value);
        }
    });
    var list = {listId: currentListId, listName: listName, words: wordNames};
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'statusCode': {
            202: function (data) {
                $('#createForm').html("<label id='nameLabel'>Name</label><input id='name'> <label>Words</label><ul id = 'newWords'> " +
                    "<li class='wordInput mdl-list__item'><span class='mdl-list__item-primary-content'><input class = 'createInput'> " +
                    "<button class='minus  mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect'>" +
                    "<i class='material-icons'>remove</i></button></span></li> </ul><label id ='warning'></label>");
                showAllWords(data.id, data.name);
                var list = $('li[id = ' + currentListId + ']');
                (list).attr("name", data.name);
                list.text(data.name);
                $('#addModal').hide();
            },
            406: function () {
                //$('#addModal').show();
                $('#warning').text("List with this name already exists")
            }
        },
        'type': 'PUT',
        'url': "/list/change",
        'data': JSON.stringify(list),
        'dataType': 'json'
    });
});


$(document).on('click', '#change', function () {
    $('#addModal').show();
    $('#save').hide();
    $('#saveChange').show();
    $('#header').text("Change list");
    var name = $('#list_name').text();
    $('#name').val(name);
    $('.wordInput').remove();
    $(".word_text").each(function (index, value) {
        $('#newWords').append("<li class = 'wordInput mdl-list__item'>" +
            "<span class='mdl-list__item-primary-content'><input class = 'createInput' value = '" + value.innerHTML + "'/>" +
            " <button class='minus  mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect'>" +
            "<i class='material-icons'>remove</i></button></span></li>");
    });
});

$(document).on('click', ".close", function () {
    $('#addModal').hide();
    $('#createForm').html("<label id='nameLabel'>Name</label><input id='name'/><label>Words</label><ul id = 'newWords'>" +
        "<li class='wordInput mdl-list__item'><span class='mdl-list__item-primary-content'>" +
        "<input class = 'createInput'/> " +
        "<button class='minus  mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect'>" +
        "<i class='material-icons'>remove</i></button></span></li></ul><label id ='warning'></label>");

});

$(document).on('click', '#addBt', function () {
    $('#addModal').show();
    $('#saveChange').hide();
    $('#save').show();
    $('#header').text("Create new list");
//counter = 1;
});


$(document).on('click', '#plus', function () {
    $('#newWords').append("<li class = 'wordInput mdl-list__item'>" +
        "<span class='mdl-list__item-primary-content'><input class = 'createInput'/> " +
        "<button class='minus  mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-js-ripple-effect'>" +
        "<i class='material-icons'>remove</i></button></span></li>");
});

//    $('#minus').click(function () {
//        $(".wordInput").last().remove();
//    });

$(document).on('click', ".minus", function () {
//$(".minus").click(function () {
    $(this).parent().parent().remove();
//$(".wordInput").last().remove();
});
//$.postJSON = function (url, data, callback, statusCodeFunction) {
//    return jQuery.ajax({
//        headers: {
//            'Accept': 'application/json',
//            'Content-Type': 'application/json'
//        },
//        'statusCode': statusCodeFunction,
//        'type': 'POST',
//        'url': url,
//        'data': JSON.stringify(data),
//        'dataType': 'json',
//        'success': callback
//    });
//};

