package controller;

import model.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import search.SearchResult;
import service.TextService;

import java.util.List;

/**
 * Created by Ольга on 02.05.2016.
 */
@RestController
public class TextController {

    @Autowired
    private TextService textService;

    @RequestMapping(value = "/texts", method = RequestMethod.GET)
    public List<Text> doGetLists() {
        return textService.getAllTextsForUser();
    }

    @RequestMapping(value = "/list/{list_id}/texts/add", method = RequestMethod.PUT)
    public List<Text> putNewTexts(@PathVariable("list_id") int listId, List<SearchResult> texts) {
        return textService.addNewTexts(listId, texts);
    }

    @RequestMapping (value = "/text/{text_id}/delete", method = RequestMethod.DELETE)
    public boolean deleteText(@PathVariable("text_id") int textId) {
        return textService.deleteTextById(textId);
    }
}
