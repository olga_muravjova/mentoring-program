package controller;

import controller.form.UserForm;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.authorisation.service.UserService;

/**
 * Created by Ольга on 10.03.2016.
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * @param userForm
     * @return HttpStatus.CONFLICT if passwords don't match
     * HttpStatus.NOT_ACCEPTABLE if user with this userName already exists
     * HttpStatus.ACCEPTED if user successfully created
     */
    @RequestMapping(value = "/user/new", method = RequestMethod.POST)
    public ResponseEntity<User> handleUserCreateForm(@RequestBody UserForm userForm) {
        if (!userForm.getPassword().equals( userForm.getRepeatPassword())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        User user = userService.createUser(userForm.getLogin(), userForm.getPassword());
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<>(user, HttpStatus.ACCEPTED);
    }

    @RequestMapping("/user/{id}")
    public ResponseEntity<User> getUserInfo(@PathVariable int id) {
        User user = userService.getUserById(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping("/user/current/name")
    public String getUserName() {
        return userService.getCurrentUserName();
    }

    @RequestMapping("/user/current/id")
    public int getUserId() {
        return userService.getCurrentUserId();
    }
}
