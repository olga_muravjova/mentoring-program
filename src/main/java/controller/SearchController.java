package controller;

import model.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import search.SearchResult;
import search.SearchService;

import java.util.List;

/**
 * Created by Ольга on 07.04.2016.
 */
@RestController
public class SearchController {

//    @Autowired
//    private WikipediaRestService wikipediaRestService;
//
//    @Autowired
//    private NewYorkTimesRestService newYorkTimesRestService;

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/search/{list_id}", method = RequestMethod.GET)
    public List<SearchResult> getWikipediaHtml(@PathVariable("list_id") int listId) {
        return searchService.search(listId);
    }

    /**
     *temporary
     */
//    @RequestMapping(value = "/search/wiki/{words}", method = RequestMethod.GET)
//    public String getWikipediaHtml(@PathVariable("words") String words) {
//        String content = wikipediaRestService.getPage(words);
//        return content;
//    }

    /**
     *temporary
     */
//    @RequestMapping(value = "/search/times/{words}", method = RequestMethod.GET)
//    public ArticleInfo getArticleFromTimes(@PathVariable("words") String words) {
//        return newYorkTimesRestService.getArticle(words);
//    }
}
