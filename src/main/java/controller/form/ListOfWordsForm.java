package controller.form;

import java.util.List;

/**
 * Created by Ольга on 06.03.2016.
 */
public class ListOfWordsForm {

    private int listId;
    private String listName;
    private List<String> words;
    //private List<String> deletedWords;


    public ListOfWordsForm(int listId, String listName, List<String> words) {
        this.listId = listId;
        this.listName = listName;
        this.words = words;
    }

//    public ListOfWordsForm(int listId, String listName, List<String> addedWords, List<String> deletedWords) {
//        this.listId = listId;
//        this.listName = listName;
//        this.addedWords = addedWords;
//        this.deletedWords = deletedWords;
//    }

    public ListOfWordsForm() {
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

//    public List<String> getAddedWords() {
//        return addedWords;
//    }
//
//    public void setAddedWords(List<String> addedWords) {
//        this.addedWords = addedWords;
//    }
//
//    public List<String> getDeletedWords() {
//        return deletedWords;
//    }
//
//    public void setDeletedWords(List<String> deletedWords) {
//        this.deletedWords = deletedWords;
//    }

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }
}
