package controller;

import controller.form.ListOfWordsForm;
import model.ListOfWords;
import model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.ListService;

import java.util.List;

/**
 * Created by Ольга on 23.02.2016.
 */
@RestController
public class ListController {

    @Autowired
    private ListService listService;

    @RequestMapping(value = "/lists", method = RequestMethod.GET)
    public List<ListOfWords> doGetLists() {
        return listService.getAllListsForUser();
    }

    @RequestMapping(value = "/list/{list_id}/words", method = RequestMethod.GET)
    public List<Word> doGetWords(@PathVariable("list_id") int listId) {
        return listService.getAllWordsByListId(listId);
    }

//    @RequestMapping(value = "/word/{word_id}/tags", method = RequestMethod.GET)
//    public List<Tag> doGetTags(@PathVariable("word_id") int wordId) {
//        return mainService.getAllTagsByWordId(wordId);
//    }

    @RequestMapping(value = "/list/erase", method = RequestMethod.DELETE)
    public void deleteList(@RequestBody int listId) {
        listService.deleteList(listId);
    }

    /**
     * @param list
     * @return HttpStatus.NOT_ACCEPTABLE if list with this name already exists
     * HttpStatus.ACCEPTED if list was created successfully
     */
    @RequestMapping(value = "/list/new", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            headers = {"content-type=application/json"})
    public ResponseEntity<ListOfWords> createNewList(@RequestBody ListOfWordsForm list) {
        return getHttpStatus(listService.createNewList(list), list.getListId());
    }

    /**
     * @param list
     * @return HttpStatus.NOT_ACCEPTABLE if list with new name already exists
     * HttpStatus.ACCEPTED if list was changed successfully
     */
    @RequestMapping(value = "/list/change", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            headers = {"content-type=application/json"})
    public ResponseEntity<ListOfWords> changeList(@RequestBody ListOfWordsForm list) {
        return getHttpStatus(listService.updateList(list), list.getListId());
    }

    private ResponseEntity<ListOfWords> getHttpStatus(ListService.Result result, int listId) {
        switch (result) {
            case SUCCESS:
                return new ResponseEntity<>(listService.getListById(listId), HttpStatus.ACCEPTED);
            default:
                return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
//            default:
//                return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}