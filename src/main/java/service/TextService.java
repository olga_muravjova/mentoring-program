package service;

import dao.ListDao;
import dao.TextDao;
import dao.UserDao;
import model.ListOfWords;
import model.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import search.SearchResult;
import search.TypeOfSource;
import service.authorisation.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ����� on 16.04.2016.
 */
@Service
public class TextService {

    @Autowired
    private TextDao textDao;

    @Autowired
    private ListDao listDao;

    @Autowired
    private UserService userService;

    public List<Text> addNewTexts(int listId, List<SearchResult> results) {
        List<Text> texts = new ArrayList<>();
        for (SearchResult result : results) {
            texts.add(textDao.addText(listId, result.getTitle(), result.getUrl(), result.getText(), result.getType()));
        }
        return texts;
    }

    public List<Text> getTextsByListId(int listId, TypeOfSource type) {
        List<Text> texts = textDao.getTextsByListId(listId);
        List<Text> textsOfCorrectType = new ArrayList<>();
        for(Text text : texts) {
            if(text.getType() == type) {
                textsOfCorrectType.add(text);
            }
        }
        return texts;
    }

    public List<Text> getTextsByListId(int listId) {
        return textDao.getTextsByListId(listId);
    }

    public List<Text> getAllTextsForUser() {
        List<ListOfWords> lists = listDao.getListsByUserId(userService.getCurrentUserId());
        List<Text> texts = new ArrayList<>();
        for(ListOfWords list : lists) {
            texts.addAll(textDao.getTextsByListId(list.getId()));
        }
        return texts;
    }

    public boolean deleteTextById(int id) {
        return textDao.deleteTextById(id);
    }
}
