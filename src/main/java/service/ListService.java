package service;

import controller.form.ListOfWordsForm;
import dao.ListDao;
import dao.TagDao;
import dao.WordDao;
import model.ListOfWords;
import model.Tag;
import model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.authorisation.service.UserService;

import java.util.List;

/**
 * Created by Ольга on 02.02.2016.
 */
@Service
public class ListService {


    @Autowired
    private ListDao listDao;

    @Autowired
    private WordDao wordDao;

    @Autowired
    private TagDao tagDao;

    @Autowired
    private UserService userService;

    public Result createNewList(ListOfWordsForm listOfWords) {
        ListOfWords list = listDao.addList(listOfWords.getListName(), userService.getCurrentUserId());
        if (list == null) {
            return Result.WRONG_NAME;
        }
        listOfWords.setListId(list.getId());
        addWordsToList(listOfWords);
        return Result.SUCCESS;
    }

    //todo remove duplicates
    private void addWordsToList(ListOfWordsForm listOfWords) {
        for (String name : listOfWords.getWords()) {
            Word word = wordDao.getWordByName(name);
            if (word == null) {
                word = wordDao.addWord(name);
            }
            listDao.addElementToList(listOfWords.getListId(), word.getId());
        }
    }

    private void deleteWordsFromList(ListOfWordsForm list) {
        for (Word word : wordDao.getWordsByListId(list.getListId())) {
            listDao.deleteElementFromList(list.getListId(), word.getId());
        }
    }

    public ListOfWords getListById(int listId) {
        return listDao.getListById(listId);
    }

    public boolean deleteList(int listId) {
        return listDao.deleteList(listId);
    }

    public List<ListOfWords> getAllListsForUser() {
        return listDao.getListsByUserId(userService.getCurrentUserId());
    }

    public List<Tag> getAllTagsByWordId(int wordId) {
        if (wordDao.getWordById(wordId) == null) {
            return null;
        }
        return tagDao.getAllTags(wordId);
    }

    public List<Word> getAllWordsByListId(int listId) {
        return wordDao.getWordsByListId(listId);
    }

    public Result updateList(ListOfWordsForm listOfWords) {
        ListOfWords list = listDao.updateListName(listOfWords.getListId(), listOfWords.getListName());
        if (list == null) {
            return Result.WRONG_NAME;
        }
        deleteWordsFromList(listOfWords);
        addWordsToList(listOfWords);
        return Result.SUCCESS;
    }

    public enum Result {
        SUCCESS,
        WRONG_NAME,
    }

    public Tag createNewTag(Tag tag) {
        return tagDao.addTag(tag);
    }

    public ListOfWords getListByName(String name) {
        return listDao.getListByName(name, userService.getCurrentUserId());
    }
}