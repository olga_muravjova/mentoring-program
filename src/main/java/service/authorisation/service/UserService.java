package service.authorisation.service;

import dao.UserDao;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

/**
 * Created by ����� on 10.03.2016.
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public User createUser(String login, String password) {
        try {
            return userDao.addUser(login, new BCryptPasswordEncoder().encode(password));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean deleteUser(int userId) {
        try {
            return userDao.deleteUser(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getCurrentUserId() {
        return ((CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
    }

    public String getCurrentUserName() {
        return ((CurrentUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    }

    public User getUserByName(String userName) {
        return userDao.getUserByName(userName);
    }

    public User getUserById(int userId) {
        return userDao.getUserById(userId);
    }

}
