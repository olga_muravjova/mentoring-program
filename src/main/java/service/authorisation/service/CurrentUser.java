package service.authorisation.service;

import model.User;
import org.springframework.security.core.authority.AuthorityUtils;

/**
 * Created by ����� on 10.03.2016.
 */
public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user) {
        super(user.getName(), user.getPassword(), AuthorityUtils.createAuthorityList("user"));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public int getId() {
        return user.getId();
    }


}