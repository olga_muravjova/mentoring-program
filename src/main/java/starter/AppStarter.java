package starter;

import config.ApplicationConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * Created by Ольга on 10.02.2016.
 */
@SpringBootApplication

public class AppStarter {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
    }
}
