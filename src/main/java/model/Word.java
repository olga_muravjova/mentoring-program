package model;

/**
 * Created by ����� on 27.11.2015.
 */
public class Word {
    private String name;
    private int id;

    public Word() {
    }

    public Word(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
