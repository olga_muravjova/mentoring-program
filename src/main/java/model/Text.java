package model;

import search.TypeOfSource;

public class Text {

    private int id;
    private int listId;
    private String title;
    private String url;
    private String text;
    private TypeOfSource type;

    public Text(int id, int listId, String title, String url, String text, TypeOfSource type) {
        this.id = id;
        this.listId = listId;
        this.title = title;
        this.url = url;
        this.text = text;
        this.type = type;
    }

    public TypeOfSource getType() {
        return type;
    }

    public void setType(TypeOfSource type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
