package model;

/**
 * Created by ����� on 27.11.2015.
 */
public class Tag {
    private String name;
    private int id;

    public Tag(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public Tag() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
