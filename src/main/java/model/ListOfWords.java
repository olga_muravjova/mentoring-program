package model;

/**
 * Created by ����� on 27.11.2015.
 */
public class ListOfWords {
    private int id;
    private String name;
    private int userId;

    public ListOfWords() {
    }

    public ListOfWords(int id, String name, int userId) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
