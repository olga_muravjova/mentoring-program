package dao;

import model.ListOfWords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Ольга on 29.11.2015.
 */
@Repository
public class ListDao {

    private static final Logger logger = LoggerFactory.getLogger(ListDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String ADD_STATEMENT = "INSERT INTO LE.Lists_Of_Words (name, user_id) VALUES(?,?)";
    private static final String BIND_STATEMENT = "INSERT INTO LE.WORDS_AND_LISTS (list_id, word_id) VALUES(?,?)";
    private static final String DELETE_STATEMENT = "DELETE FROM LE.Lists_Of_Words Values where id = ?";
    private static final String FIND_BY_NAME_STATEMENT = "SELECT * FROM LE.Lists_Of_Words WHERE name = ? and user_id = ?";
    private static final String FIND_BY_ID_STATEMENT = "SELECT * FROM LE.Lists_Of_Words WHERE id = ?";
    private static final String DELETE_ELEMENT_STATEMENT = "DELETE FROM LE.WORDS_AND_LISTS where list_id = ? AND word_id = ?";
    private static final String GET_BY_USER_ID_STATEMENT = "SELECT * FROM LE.Lists_Of_Words WHERE user_id = ?";
    private static final String UPDATE_STATEMENT = "UPDATE LE.LISTS_OF_WORDS SET name = ? WHERE id =?;";

    public ListOfWords addList(final String listName, final int userId) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int listId;
        try {
            PreparedStatementCreator preparedStatementCreator = con -> {
                PreparedStatement pst =
                        con.prepareStatement(ADD_STATEMENT, new String[]{"id"});
                pst.setString(1, listName);
                pst.setInt(2, userId);
                return pst;
            };
            jdbcTemplate.update(preparedStatementCreator, keyHolder);
            listId = keyHolder.getKey().intValue();
        } catch (DuplicateKeyException e) {
            logger.error("", e);
            return null;
        }
        return new ListOfWords(listId, listName, userId);
    }

    public boolean deleteList(int listId) {
        return jdbcTemplate.update(DELETE_STATEMENT, listId) != 0;
    }

    public boolean addElementToList(int listId, int wordId) {
//        TransactionDefinition def = new DefaultTransactionDefinition();
//        TransactionStatus status = txManager.getTransaction(def);
        try {
            jdbcTemplate.update(BIND_STATEMENT, listId, wordId);
        } catch (Exception e) {
            logger.error("", e);
            //txManager.rollback(status);
            return false;
        }
        //txManager.commit(status);
        return true;
    }

    public void addElementsToList(int listId, int[] wordIds) {
        for (int wordId : wordIds) {
            try {
                jdbcTemplate.update(BIND_STATEMENT, listId, wordId);
            } catch (Exception e) {
                logger.error("", e);
                continue;
            }
        }
//        TransactionDefinition def = new DefaultTransactionDefinition();
//        TransactionStatus status = txManager.getTransaction(def);
//
//        txManager.commit(status);
        // return true;
    }

    public boolean deleteElementFromList(int listId, int wordId) {
        return jdbcTemplate.update(DELETE_ELEMENT_STATEMENT, listId, wordId) != 0;
    }

    public ListOfWords getListByName(String name, int userId) {
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(FIND_BY_NAME_STATEMENT, name, userId);
        if (rowSet.first()) {
            int id = rowSet.getInt("id");
            //User user = userDao.getUserById(rowSet.getInt("user_id"));
            return new ListOfWords(id, name, userId);
        }
        return null;
        // return jdbcTemplate.queryForObject(FIND_BY_NAME_STATEMENT, new Object[]{name, userId}, new ListRowMapper());
    }

    public ListOfWords getListById(int id) {
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(FIND_BY_ID_STATEMENT, id);
        if (rowSet.first()) {
            String name = rowSet.getString("name");
            int userId = rowSet.getInt("user_id");
            return new ListOfWords(id, name, userId);
        }
        return null;
        //return jdbcTemplate.queryForObject(FIND_BY_ID_STATEMENT, new Object[]{id}, new ListRowMapper());
    }

    public ListOfWords updateListName(int id, String name) {
        try {
            jdbcTemplate.update(UPDATE_STATEMENT, name, id);
        } catch (Exception e) {
            logger.error("", e);
            return null;
        }
        return getListById(id);
    }


    public List<ListOfWords> getListsByUserId(int userId) {
        return jdbcTemplate.query(GET_BY_USER_ID_STATEMENT, new Object[]{userId}, new ListRowMapper());
//        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(GET_BY_USER_ID_STATEMENT, userId);
//        while (rowSet.next()) {
//            lists.add(getListById(rowSet.getInt("id")));
//        }
//        return lists;
    }

    private static class ListRowMapper implements RowMapper<ListOfWords> {

        @Override
        public ListOfWords mapRow(ResultSet resultSet, int i) throws SQLException {
            ListOfWords listOfWords = new ListOfWords();
            listOfWords.setId(resultSet.getInt("id"));
            listOfWords.setName(resultSet.getString("name"));
            listOfWords.setUserId(resultSet.getInt("user_id"));
            return listOfWords;
        }
    }
}
