package dao;

import model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ольга on 27.11.2015.
 */

@Repository
public class TagDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String ADD_STATEMENT = "INSERT INTO LE.Tags (name) VALUES(?)";
    private static final String DELETE_STATEMENT = "DELETE FROM LE.Tags Values where id = ?";
    private static final String GET_STATEMENT = "SELECT * FROM LE.Tags WHERE id = ?";
    private static final String GET_TAGS_BY_WORD_ID_STATEMENT = "SELECT * FROM LE.tags_and_words WHERE word_id = ?";

    public Tag addTag(Tag tag) {
        try {
            jdbcTemplate.update(ADD_STATEMENT, tag.getName());
        } catch (Exception e) {
            return null;
        }
        return tag;
    }


    public Tag deleteTag(Tag tag) {
        int rowsAffected;
        try {
            rowsAffected = jdbcTemplate.update(DELETE_STATEMENT, tag.getId());
        } catch (Exception e) {
            return null;
        }
        if (rowsAffected > 0) {
            return tag;
        }
        return null;
    }

//    public Tag getThemeByName(String name) {
//        return jdbcTemplate.queryForObject("SELECT * FROM LEARNING_ENGLISH.Themes WHERE NAME = ?", new Object[]{name}, new TagRowMapper());
//    }

    public List<Tag> getAllTags(int wordId) {
        List<Tag> tags = new ArrayList<Tag>();
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(GET_TAGS_BY_WORD_ID_STATEMENT, wordId);
        while (rowSet.next()) {
            tags.add(getTagById(rowSet.getInt("id")));
        }
//        try {
//
//        }catch (Exception e) {
//            return null;
//        }
        return tags;
    }

    public Tag getTagById(int id) {
        return jdbcTemplate.queryForObject(GET_STATEMENT, new Object[]{id}, new TagRowMapper());
    }

    private static class TagRowMapper implements RowMapper<Tag> {
        @Override
        public Tag mapRow(ResultSet resultSet, int i) throws SQLException {
            Tag tag = new Tag();
            tag.setId(resultSet.getInt("id"));
            tag.setName(resultSet.getString("name"));
            return tag;
        }
    }

}
