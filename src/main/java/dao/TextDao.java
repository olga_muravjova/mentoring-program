package dao;

import model.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import search.TypeOfSource;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ����� on 16.04.2016.
 */
@Repository
public class TextDao {

    private static final Logger logger = LoggerFactory.getLogger(ListDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String ADD_STATEMENT = "INSERT INTO LE.Texts (list_id, title,url,text,type) VALUES(?,?,?,?,?)";
    private static final String FIND_BY_ID_STATEMENT = "SELECT * FROM LE.Texts WHERE id = ?";
    private static final String GET_ELEMENTS_STATEMENT = "SELECT * FROM LE.Texts WHERE list_id = ?";
    private static final String DELETE_ELEMENTS_STATEMENT = "DELETE FROM LE.Texts where list_id =?";
    private static final String DELETE_BY_ID_STATEMENT = "DELETE FROM LE.Texts where id =?";


    public Text getTextById(int id) {
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(FIND_BY_ID_STATEMENT, id);
        if (rowSet.first()) {
            int listId = rowSet.getInt("list_id");
            String title = rowSet.getString("title");
            String url = rowSet.getString("url");
            String text = rowSet.getString("text");
            String typeString = rowSet.getString("type");
            TypeOfSource type;
            if (typeString.equals("WIKIPEDIA")) {
                type = TypeOfSource.WIKIPEDIA;
            } else if (typeString.equals("TIMES")) {
                type = TypeOfSource.TIMES;
            } else {
                throw new IllegalArgumentException();
            }
            return new Text(id, listId, title, url, text, type);
        }
        return null;
    }

    public List<Text> getTextsByListId(int listId) {
        List<Text> texts = new ArrayList<>();
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(GET_ELEMENTS_STATEMENT, listId);
        while (rowSet.next()) {
            int id = rowSet.getInt("id");
            String title = rowSet.getString("title");
            String url = rowSet.getString("url");
            String text = rowSet.getString("text");
            String typeString = rowSet.getString("type");
            TypeOfSource type;
            if (typeString.equals("WIKIPEDIA")) {
                type = TypeOfSource.WIKIPEDIA;
            } else if (typeString.equals("TIMES")) {
                type = TypeOfSource.TIMES;
            } else {
                throw new IllegalArgumentException();
            }
            texts.add(new Text(id, listId, title, url, text, type));
        }
        return texts;
    }

    public Text addText(final int listId, String title, String url, String text, TypeOfSource type) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int textId;
        try {
            PreparedStatementCreator preparedStatementCreator = con -> {
                PreparedStatement pst =
                        con.prepareStatement(ADD_STATEMENT, new String[]{"id"});
                pst.setInt(1, listId);
                pst.setString(2, title);
                pst.setString(3, url);
                pst.setString(4, text);
                pst.setString(5, type.toString());
                return pst;
            };
            jdbcTemplate.update(preparedStatementCreator, keyHolder);
            textId = keyHolder.getKey().intValue();
        } catch (DuplicateKeyException e) {
            logger.error("", e);
            return null;
        }
        return new Text(textId, listId, title, url, text, type);
    }

    public boolean deleteAllTextsByListId(int listId) {
        return jdbcTemplate.update(DELETE_ELEMENTS_STATEMENT, listId) != 0;
    }

    public boolean deleteTextById(int textId) {
        return jdbcTemplate.update(DELETE_BY_ID_STATEMENT, textId) != 0;
    }

}
