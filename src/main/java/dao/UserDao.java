package dao;

import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ����� on 27.11.2015.
 */
@Repository
public class UserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String ADD_STATEMENT = "INSERT INTO LE.Users (name, password) VALUES(?, ?)";
    private static final String DELETE_STATEMENT = "DELETE FROM LE.Users Values where id = ?";
    private static final String GET_STATEMENT = "SELECT * FROM LE.Users WHERE id = ?";
    private static final String GET_BY_NAME_AND_PASSWORD_STATEMENT = "SELECT * FROM LE.Users WHERE name = ? AND password = ?";
    private static final String GET_BY_NAME_STATEMENT = "SELECT * FROM LE.Users WHERE name = ?";

    public User addUser(String userName, String password) throws SQLException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int userId;
        try {
            PreparedStatementCreator preparedStatementCreator = con -> {
                PreparedStatement pst =
                        con.prepareStatement(ADD_STATEMENT, new String[]{"id"});
                pst.setString(1, userName);
                pst.setString(2, password);
                return pst;
            };
            jdbcTemplate.update(preparedStatementCreator, keyHolder);
            userId = keyHolder.getKey().intValue();
        } catch (DuplicateKeyException e) {
            logger.error("", e);
            return null;
        }
        return new User(userId, userName, password);
    }

    public boolean deleteUser(int userId) throws SQLException {
        return jdbcTemplate.update(DELETE_STATEMENT, userId) != 0;
    }

    public User getUserById(int id) {
        return jdbcTemplate.queryForObject(GET_STATEMENT, new Object[]{id}, new UserRowMapper());
    }

    public User getUserByNameAndPassword(String name, String password) {
        return jdbcTemplate.queryForObject(GET_BY_NAME_AND_PASSWORD_STATEMENT, new Object[]{name, password}, new UserRowMapper());
    }

    public User getUserByName(String name) {
        return jdbcTemplate.queryForObject(GET_BY_NAME_STATEMENT, new Object[]{name}, new UserRowMapper());
    }

    private static class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setPassword(resultSet.getString("password"));
            return user;
        }
    }
}
