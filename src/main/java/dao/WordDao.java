package dao;

import model.Word;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ольга on 29.11.2015.
 */
@Repository
public class WordDao {

    private static final Logger logger = LoggerFactory.getLogger(WordDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String ADD_STATEMENT = "INSERT INTO LE.Words (name) VALUES(?)";
    private static final String DELETE_STATEMENT = "DELETE FROM LE.Words where id = ?";
    private static final String GET_BY_ID_STATEMENT = "SELECT * FROM LE.Words WHERE id = ?";
    //private static final String bindStatement = "INSERT INTO LE.tags_and_Words (word_id, tag_id) VALUES(?,?)";
    //private static final String deleteElementStatement = "DELETE FROM LE.tags_and_Words where word_id = ? AND tag_id = ?";
    private static final String GET_BY_NAME_STATEMENT = "SELECT * FROM LE.Words WHERE NAME = ?";
    private static final String GET_ELEMENTS_STATEMENT = "SELECT * FROM LE.WORDS_AND_LISTS WHERE list_id = ?";

    /**
     * non-existence should be checked before!
     *
     * @param word
     * @return
     * @throws SQLException
     */
    public Word addWord(final String word) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int wordId;
        try {
            PreparedStatementCreator preparedStatementCreator = con -> {
                PreparedStatement pst =
                        con.prepareStatement(ADD_STATEMENT, new String[]{"id"});
                pst.setString(1, word);
                return pst;
            };
            jdbcTemplate.update(preparedStatementCreator, keyHolder);
            wordId = keyHolder.getKey().intValue();
        }
        catch (DuplicateKeyException e) {
            logger.error("", e);
            return null;
        }
        return new Word(wordId, word);
    }

    public boolean deleteWord(Word word) {
        return jdbcTemplate.update(DELETE_STATEMENT, word.getId()) != 0;
    }

    public Word getWordByName(String word) {
        //return jdbcTemplate.queryForObject(GET_BY_NAME_STATEMENT, new Object[]{word}, new WordRowMapper());
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(GET_BY_NAME_STATEMENT, word);
        if (rowSet.first()) {
            int id = rowSet.getInt("id");
            return new Word(id, word);
        }
        return null;
    }

    public Word getWordById(int id) {
        //return jdbcTemplate.queryForObject(GET_BY_ID_STATEMENT, new Object[]{id}, new WordRowMapper());

        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(GET_BY_ID_STATEMENT, id);
        if (rowSet.first()) {
            String word = rowSet.getString("name");
            return new Word(id, word);
        }
        return null;
    }
//
//
//    public Word addTagToWord(Word word, Tag tag) {
//        try {
//            if (tagDao.getTagById(tag.getId()) == null) {
//                return null;
//            }
//            jdbcTemplate.update(bindStatement, word.getId(), tag.getId());
//        } catch (Exception e) {
//            return null;
//        }
//        return word;
//    }
//
//    public Word deleteTagFromWord(Word word, Tag tag) {
//        try {
//            if (tagDao.getTagById(tag.getId()) == null) {
//                return null;
//            }
//            jdbcTemplate.update(deleteElementStatetmet, word.getId(), tag.getId());
//        } catch (Exception e) {
//            return null;
//        }
//        return word;
//    }


    public List<Word> getWordsByListId(int id) {
        // return jdbcTemplate.query(GET_ELEMENTS_STATEMENT, new Object[]{id}, new WordRowMapper());
        List<Word> words = new ArrayList<>();
        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(GET_ELEMENTS_STATEMENT, id);
        while (rowSet.next()) {
            words.add(getWordById(rowSet.getInt("word_id")));
        }
        return words;
    }

//    private static class WordAndListsRowMapper implements RowMapper<Word> {
//
//        @Override
//        public Word mapRow(ResultSet resultSet, int i) throws SQLException {
//            Word word = new Word();
//            word.setId(resultSet.getInt("word_id"));
//            word.setName(getWordById(resultSet.getInt("word_id"));
//            return word;
//        }
//    }

    private static class WordRowMapper implements RowMapper<Word> {

        @Override
        public Word mapRow(ResultSet resultSet, int i) throws SQLException {
            Word word = new Word();
            word.setId(resultSet.getInt("id"));
            word.setName(resultSet.getString("name"));
            return word;
        }
    }
}
