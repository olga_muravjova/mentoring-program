package search.api.service.times.response.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 02.05.2016.
 */
public class Meta {
    @JsonProperty("hits")
    private int hits;

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }
}
