package search.api.service.times.response.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 09.04.2016.
 */
public class ResponseFromTimesType {
@JsonProperty("response")
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
