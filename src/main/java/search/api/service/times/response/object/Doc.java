package search.api.service.times.response.object;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

/**
 * Created by Ольга on 09.04.2016.
 */
public class Doc {
    @JsonProperty("web_url")
    private String web_url;
    @JsonProperty("lead_paragraph")
    private String lead_paragraph;
    @JsonProperty("pub_date")
    private Date date;
    @JsonProperty("headline")
    private Headline headline;

    public Headline getHeadline() {
        return headline;
    }

    public void setHeadline(Headline headline) {
        this.headline = headline;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }

    public String getLead_paragraph() {
        return lead_paragraph;
    }

    public void setLead_paragraph(String lead_paragraph) {
        this.lead_paragraph = lead_paragraph;
    }
}
