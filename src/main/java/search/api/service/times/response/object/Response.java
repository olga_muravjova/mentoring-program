package search.api.service.times.response.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 09.04.2016.
 */
public class Response {

    @JsonProperty("meta")
    private Meta meta;

    @JsonProperty("docs")
    private Doc[] docs;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Doc[] getDocs() {
        return docs;
    }

    public void setDocs(Doc[] docs) {
        this.docs = docs;
    }
}
