package search.api.service.times.response.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 16.04.2016.
 */
public class Headline {
    @JsonProperty("main")
    private String main;

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }
}
