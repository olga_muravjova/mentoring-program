package search.api.service.times;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import search.SearchResult;
import search.TypeOfSource;
import search.api.service.IApiService;
import search.api.service.times.response.object.ResponseFromTimesType;

/**
 * Created by Ольга on 09.04.2016.
 */
@Service
public class NewYorkTimesRestService implements IApiService {

    private RestTemplate restTemplate = new RestTemplate();
    private final static String QUERY_STRING =
            "http://api.nytimes.com/svc/search/v2/articlesearch.json?" +
                    "api-key=a85fa3baed03c0f915d12b40fe3dd5b5:18:74961260&sort=newest&q=";

    public SearchResult getArticle(String words) {
        ResponseFromTimesType r = restTemplate.getForObject(QUERY_STRING + words, ResponseFromTimesType.class);
        if (r.getResponse().getDocs().length == 0) {
            return null;
        }
        return new SearchResult(TypeOfSource.TIMES, r.getResponse().getDocs()[0].getHeadline().getMain(),
                r.getResponse().getDocs()[0].getLead_paragraph(),
                r.getResponse().getDocs()[0].getWeb_url(), r.getResponse().getMeta().getHits());
    }
}
