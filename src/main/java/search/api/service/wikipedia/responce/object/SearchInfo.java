package search.api.service.wikipedia.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 02.05.2016.
 */
public class SearchInfo {
    @JsonProperty("totalhits")
    private int totalhits;

    public int getTotalhits() {
        return totalhits;
    }

    public void setTotalhits(int totalhits) {
        this.totalhits = totalhits;
    }
}
