package search.api.service.wikipedia.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 07.04.2016.
 */
public class SearchObject {
    @JsonProperty("title")
    private String title;
    @JsonProperty("wordcount")
    private int wordcount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getWordcount() {
        return wordcount;
    }

    public void setWordcount(int wordcount) {
        this.wordcount = wordcount;
    }

}
