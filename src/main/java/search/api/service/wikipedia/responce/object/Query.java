package search.api.service.wikipedia.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 07.04.2016.
 */
public class Query {
    @JsonProperty("search")
    private SearchObject[] search;

    @JsonProperty("searchinfo")
    private SearchInfo searchinfo;


    public SearchInfo getSearchInfo() {
        return searchinfo;
    }

    public SearchInfo getSearchinfo() {
        return searchinfo;
    }

    public void setSearchinfo(SearchInfo searchinfo) {
        this.searchinfo = searchinfo;
    }

    public void setSearchInfo(SearchInfo searchInfo) {
        this.searchinfo = searchInfo;
    }

    public SearchObject[] getSearch() {
        return search;
    }

    public void setSearch(SearchObject[] search) {
        this.search = search;
    }
}
