package search.api.service.wikipedia.text.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 17.04.2016.
 */
public class Page {

    @JsonProperty("extract")
    private String extract;

    @JsonProperty("fullurl")
    private String url;

    public String getExtract() {
        return extract;
    }

    public void setExtract(String extract) {
        this.extract = extract;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
