package search.api.service.wikipedia.text.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Map;

/**
 * Created by Ольга on 17.04.2016.
 */
public class Query {
    @JsonProperty("pages")
    private Map<String, Page> pages;

    public Map<String, Page> getPages() {
        return pages;
    }

    public void setPages(Map<String, Page> pages) {
        this.pages = pages;
    }
}
