package search.api.service.wikipedia.text.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 17.04.2016.
 */
public class PageNumber {

    //todo do smth with this property
    @JsonProperty("25391")
    private Page page;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
