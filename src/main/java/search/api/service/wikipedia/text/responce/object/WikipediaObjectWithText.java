package search.api.service.wikipedia.text.responce.object;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Ольга on 17.04.2016.
 */
public class WikipediaObjectWithText {
    @JsonProperty("query")
    private Query query;

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }
}
