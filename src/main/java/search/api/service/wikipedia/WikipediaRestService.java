package search.api.service.wikipedia;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import search.SearchResult;
import search.TypeOfSource;
import search.api.service.IApiService;
import search.api.service.wikipedia.responce.object.SearchObject;
import search.api.service.wikipedia.responce.object.WikipediaObject;
import search.api.service.wikipedia.text.responce.object.Page;
import search.api.service.wikipedia.text.responce.object.WikipediaObjectWithText;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Map;

/**
 * Created by Ольга on 02.04.2016.
 */
@Service
public class WikipediaRestService implements IApiService {

    private RestTemplate restTemplate = new RestTemplate();

    private static final String SEARCH_REQUEST = "https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srwhat=text&srsearch=";
    private static final String GET_TEXT_REQUEST = "https://en.wikipedia.org//w/api.php?action=query&format=json&prop=extracts|info&inprop=url&explaintext=&exintro=&titles=";

    public SearchResult getArticle(String words) {
        try {
            turnOffSslChecking();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        WikipediaObject w = restTemplate.getForObject(SEARCH_REQUEST + words, WikipediaObject.class);
        if (w.getQuery().getSearch().length == 0) {
            return null;
        }
        SearchObject minSearchObject = w.getQuery().getSearch()[0];
        for (SearchObject searchObject : w.getQuery().getSearch()) {
            if (searchObject.getWordcount() < minSearchObject.getWordcount()) {
                minSearchObject = searchObject;
            }
        }
        WikipediaObjectWithText wikipediaObjectWithText = restTemplate.getForObject(GET_TEXT_REQUEST + minSearchObject.getTitle(),
                WikipediaObjectWithText.class);
        Map.Entry<String, Page> entry = wikipediaObjectWithText.getQuery().getPages().entrySet().iterator().next();
        if (entry == null) {
            throw new RuntimeException("Null response from wiki");
        }
        return new SearchResult(TypeOfSource.WIKIPEDIA, minSearchObject.getTitle(),
                entry.getValue().getExtract(),
                entry.getValue().getUrl(), w.getQuery().getSearchInfo().getTotalhits());
    }

    /* First version
    / with parsing
     */
//    public SearchResult getArticle(String words) {
//        WikipediaObject w = restTemplate.getForObject
//                ("https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch=" + words + "&srwhat=text",
//                        WikipediaObject.class);
//        if (w.getQuery().getSearch().length > 0) {
//            SearchObject minSearchObject = w.getQuery().getSearch()[0];
//            for (SearchObject searchObject : w.getQuery().getSearch()) {
//                if (searchObject.getWordcount() < minSearchObject.getWordcount()) {
//                    minSearchObject = searchObject;
//                }
//            }
//            List<String> listOfTitleStrings = new ArrayList<>();
//            listOfTitleStrings.add(minSearchObject.getTitle());
//            User user = new User("", "", "http://en.wikipedia.org/w/api.php");
//            user.login();
//            List<Page> listOfPages = user.queryContent(listOfTitleStrings);
//            for (Page page : listOfPages) {
//                MyWikiModel wikiModel = new MyWikiModel("${image}", "${title}");
//                String currentContent = page.getCurrentContent();
//                String html = wikiModel.render(new MyHtmlConverter(true, true), currentContent);
//                return null;
//            }
//        }
//        return null;
//    }

    private static final TrustManager[] UNQUESTIONING_TRUST_MANAGER = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
    };

    public static void turnOffSslChecking() throws NoSuchAlgorithmException, KeyManagementException {
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, UNQUESTIONING_TRUST_MANAGER, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }
}
