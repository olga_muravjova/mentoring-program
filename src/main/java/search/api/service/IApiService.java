package search.api.service;

import search.SearchResult;

/**
 * Created by Ольга on 16.04.2016.
 */
public interface IApiService {

    SearchResult getArticle(String words);
}
