package search;

import model.Word;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import search.api.service.IApiService;
import search.api.service.times.NewYorkTimesRestService;
import search.api.service.wikipedia.WikipediaRestService;
import service.ListService;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by ����� on 16.04.2016.
 */
@Service
public class SearchService {

    @Autowired
    private ListService listService;

    private ExecutorService executorService = Executors.newCachedThreadPool();

    private List<SearchResult> results = new ArrayList<>();

    public List<SearchResult> search(int listId) {
        results.clear();
        List<Word> words = listService.getAllWordsByListId(listId);
        Future<List<SearchResult>> future1 = executorService.submit(new SearchTextTask(new WikipediaRestService(), words));
        Future<List<SearchResult>> future2 = executorService.submit(new SearchTextTask(new NewYorkTimesRestService(), words));
        try {
            results.addAll(future1.get());
            results.addAll(future2.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return results;
    }

    private class SearchTextTask implements Callable<List<SearchResult>> {
        private final IApiService iApiService;
        private final List<Word> words;

        public SearchTextTask(IApiService iApiService, List<Word> words) {
            this.iApiService = iApiService;
            this.words = words;
        }

        @Override
        public List<SearchResult> call() throws Exception {
            return searchTexts(words, iApiService);
        }
    }

    private List<SearchResult> searchTexts(List<Word> words, IApiService searchService) {
        List<SearchResult> results = new ArrayList<>();
        SearchResult result = searchService.getArticle(pasteTogetherWords(words));
        if (result != null) {
            results.add(result);
            return results;
        }
        //order words by total hits
        Map<Integer, Word> temporaryResults = new TreeMap<>();
        for (Word word : words) {
            result = searchService.getArticle(word.getName());
            if (result != null) {
                temporaryResults.put(result.getTotalHits(), word);
            }
        }
        if (temporaryResults.isEmpty()) {
            return results;
        }
        //remove words until result!= null
        Collection<Word> tempWords = temporaryResults.values();
        while (!tempWords.isEmpty()) {
            result = searchService.getArticle(pasteTogetherWords(tempWords));
            if (result != null) {
                results.add(result);
                return results;
            }
            //todo maybe add some more results here
            //results.add(searchService.getArticle(pasteTogetherWords(tempWords)))
            tempWords.remove(tempWords.iterator().next());
        }
        return results;
    }

    private String pasteTogetherWords(Collection<Word> words) {
        String wordsString = "";
        for (Word word : words) {
            wordsString += word.getName() + ' ';
        }
        return wordsString;
    }
}
