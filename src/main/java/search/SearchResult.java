package search;

/**
 * Created by Ольга on 12.04.2016.
 */
public class SearchResult {

    private TypeOfSource type;
    private String title;
    private String text;
    private String url;
    private Integer totalHits;

    public SearchResult(TypeOfSource type, String title, String text, String url, Integer totalHits) {
        this.type = type;
        this.title = title;
        this.text = text;
        this.url = url;
        this.totalHits = totalHits;
    }

    public Integer getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(Integer totalHits) {
        this.totalHits = totalHits;
    }

    public TypeOfSource getType() {
        return type;
    }

    public void setType(TypeOfSource type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
