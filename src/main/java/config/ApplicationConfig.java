package config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.sql.DataSource;

/**
 * Created by Ольга on 01.12.2015.
 */
@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"dao", "service", "controller", "config","search"})
public class ApplicationConfig {
    @Bean
    DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");
        return dataSource;
    }

    @Bean
    JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public PlatformTransactionManager txManager() {
        return new DataSourceTransactionManager(dataSource());
    }

//    @Value("${spring.view.prefix}")
//    private String prefix = "";
//
//    @Value("${spring.view.suffix}")
//    private String suffix = "";
//
//    @Value("${spring.view.view-names}")
//    private String viewNames = "";
//
//    @Bean
//    InternalResourceViewResolver jspViewResolver() {
//        final InternalResourceViewResolver vr = new InternalResourceViewResolver();
//        vr.setPrefix(prefix);
//        vr.setSuffix(suffix);
//        vr.setViewClass(JstlView.class);
//        vr.setViewNames(viewNames);
//        return vr;
//    }
}
