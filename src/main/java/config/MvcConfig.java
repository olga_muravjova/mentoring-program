package config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration

public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/resources/templates");
//        registry.addResourceHandler()
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/welcome").setViewName("start");
        registry.addViewController("/mytexts").setViewName("texts");
        registry.addViewController("/mylists").setViewName("main");
        registry.addViewController("/create").setViewName("createUser");
//        registry.addViewController("/vk").setViewName("vk");
//        registry.addViewController("/wiki").setViewName("wiki");

    }

}